package main

import (
	"go-faq/internal/app/master"
)

func main() {
	app.Run()
}