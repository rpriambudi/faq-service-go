package auth

import (
	"encoding/json"
	"errors"
	"os"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jws"
)

const jwksURL = `http://localhost/auth/realms/nest-bank-api/protocol/openid-connect/certs`

type AuthResult struct {
	Role  string
	Error error
}

func Validate(token string, c chan AuthResult) {
	res, err := jwk.FetchHTTP(os.Getenv("AUTH_JWKS_URL"))
	if err != nil {
		c <- AuthResult{Role: "", Error: err}
		return
	}
	keys := res.Keys[0]

	var pubkey interface{}
	var tokenObj map[string]interface{}
	if err := keys.Raw(&pubkey); err != nil {
		c <- AuthResult{Role: "", Error: err}
		return
	}

	verified, err := jws.Verify([]byte(token), jwa.RS256, pubkey)
	if err != nil {
		c <- AuthResult{Role: "", Error: err}
		return
	}
	json.Unmarshal(verified, &tokenObj)
	roles := tokenObj["roles"].([]interface{})
	if len(roles) == 0 {
		c <- AuthResult{Role: "", Error: errors.New("User has no roles")}
		return
	}

	c <- AuthResult{Role: roles[0].(string), Error: nil}
}
