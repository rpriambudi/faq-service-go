package esclient

import (
	"bytes"
	"context"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
)

type ESClientService interface {
	Index(index string, documentID string, body []byte) error
	Delete(index string, documentID string) error
	Search(index string, query bytes.Buffer) (interface{}, error)
}

type ESSearchService struct {
	Client *elasticsearch.Client
}

func (ss ESSearchService) Index(index string, documentID string, body []byte) error {
	req := esapi.IndexRequest{
		Index:      index,
		DocumentID: documentID,
		Body:       strings.NewReader(string(body)),
		Refresh:    "true",
	}
	_, err := req.Do(context.Background(), ss.Client)
	return err
}

func (ss ESSearchService) Delete(index string, documentID string) error {
	req := esapi.DeleteRequest{
		Index:      index,
		DocumentID: documentID,
		Refresh:    "true",
	}
	_, err := req.Do(context.Background(), ss.Client)
	return err
}

func (ss ESSearchService) Search(index string, query bytes.Buffer) (interface{}, error) {
	res, err := ss.Client.Search(
		ss.Client.Search.WithContext(context.Background()),
		ss.Client.Search.WithIndex(index),
		ss.Client.Search.WithBody(&query),
		ss.Client.Search.WithTrackTotalHits(true),
		ss.Client.Search.WithPretty(),
	)
	return res, err
}
