package migrations

import (
	"fmt"

	"github.com/go-pg/migrations/v7"
)

func init() {
	migrations.MustRegisterTx(func(db migrations.DB) error {
		fmt.Println("migrations up")
		_, err := db.Exec(`CREATE TABLE faqs (
			id serial PRIMARY KEY,
			orders int,
			question text,
			answer text,
			created_at date default now(),
			updated_at date default now()
		)`)
		return err
	}, func(db migrations.DB) error {
		fmt.Println("migrations down")
		_, err := db.Exec(`DROP TABLE IF EXISTS faqs`)
		return err
	})
}
