package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"go-faq/internal/app/master/models"
	"go-faq/internal/app/master/providers/search"
	repository "go-faq/internal/app/master/repositories"
)

// FaqController class which holds functions mapped to endpoints
type FaqController struct {
	faqrepository repository.BaseRepository
	faqsearch     search.BaseSearch
}

// NewFaqController create new instance of FaqController
func NewFaqController() FaqController {
	faqrepository := repository.RegisteredRepository["FaqRepository"].(repository.BaseRepository)
	faqsearch := search.SearchProviders["FaqSearch"].(search.BaseSearch)
	return FaqController{
		faqrepository: faqrepository,
		faqsearch:     faqsearch,
	}
}

// GetFaq method to handle request for getting FAQs entry
func (fc FaqController) GetFaq(w http.ResponseWriter, r *http.Request) {
	result, err := fc.faqrepository.Get()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonObj, err := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

// GetFaqByID ...
func (fc FaqController) GetFaqByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	faqID, ok := ctx.Value("faqId").(string)
	if !ok {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	id, err := strconv.Atoi(faqID)
	if err != nil {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	faq, err := fc.faqrepository.GetById(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonObj, err := json.Marshal(faq)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

// CreateFaq ...
func (fc FaqController) CreateFaq(w http.ResponseWriter, r *http.Request) {
	var faq *models.Faq

	err := json.NewDecoder(r.Body).Decode(&faq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	result, err := fc.faqrepository.Add(faq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	go fc.faqsearch.Index(faq)
	jsonObj, err := json.Marshal(result)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

// UpdateFaq ...
func (fc FaqController) UpdateFaq(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	faqID, ok := ctx.Value("faqId").(string)
	if !ok {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	var faq *models.Faq
	err := json.NewDecoder(r.Body).Decode(&faq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(faqID)
	if err != nil {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	result, err := fc.faqrepository.Update(id, faq)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	go fc.faqsearch.Index(faq)
	jsonObj, err := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

// DeleteFaq ...
func (fc FaqController) DeleteFaq(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	faqID, ok := ctx.Value("faqId").(string)
	if !ok {
		http.Error(w, http.StatusText(422), 422)
	}

	id, err := strconv.Atoi(faqID)
	if err != nil {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	repoErr := fc.faqrepository.Delete(id)
	if repoErr != nil {
		http.Error(w, repoErr.Error(), 500)
		return
	}

	searchErr := fc.faqsearch.Delete(faqID)
	if searchErr != nil {
		http.Error(w, searchErr.Error(), 500)
		return
	}
}

func (fc FaqController) SearchFaq(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	results, err := fc.faqsearch.Search(query)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonObj, err := json.Marshal(results)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}
