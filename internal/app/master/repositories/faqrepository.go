package repository

import (
	"go-faq/internal/app/master/models"
	"time"

	"github.com/go-pg/pg/v9"
)

type FaqRepository struct {
	db *pg.DB
}

func (fr FaqRepository) Add(i interface{}) (interface{}, error) {
	f := i.(*models.Faq)
	_, err := fr.db.Model(f).Returning("*").Insert()
	return *f, err
}

func (fr FaqRepository) Update(id int, i interface{}) (interface{}, error) {
	f := i.(*models.Faq)
	f.Id = id
	f.UpdatedAt = time.Now()
	err := fr.db.Update(f)
	return *f, err
}

func (fr FaqRepository) Delete(id int) error {
	faq := &models.Faq{
		Id: id,
	}

	err := fr.db.Delete(faq)
	return err
}

func (fr FaqRepository) Get() (interface{}, error) {
	var faqs []models.Faq
	err := fr.db.Model(&faqs).Order("created_at DESC").Select()
	return faqs, err
}

func (fr FaqRepository) GetById(id int) (interface{}, error) {
	faq := &models.Faq{Id: id}
	err := fr.db.Select(faq)
	return *faq, err
}
