package repository

import "github.com/go-pg/pg/v9"

// RegisteredRepository is an exported map of RegisteredRepository
var RegisteredRepository map[string]BaseRepository

// Initialize : this method initialize any registered
func Initialize(dbObj *pg.DB) {
	RegisteredRepository = make(map[string]BaseRepository)
	faqRepository := FaqRepository{
		db: dbObj,
	}

	RegisteredRepository["FaqRepository"] = faqRepository
}

// BaseRepository is basic interface for any repository
type BaseRepository interface {
	Get() (interface{}, error)
	GetById(id int) (interface{}, error)
	Add(i interface{}) (interface{}, error)
	Update(id int, i interface{}) (interface{}, error)
	Delete(id int) error
}
