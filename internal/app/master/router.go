package app

import (
	"context"
	"net/http"
	"strings"

	"go-faq/internal/app/master/controllers"
	"go-faq/pkg/auth"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
)

// Route is a router for all FAQ related endpoints
func Route() *chi.Mux {
	faqcontroller := controllers.NewFaqController()
	chiRoute := chi.NewRouter()

	chiRoute.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Content-Type", "Authorization"},
		AllowCredentials: true,
	}))

	chiRoute.Route("/api/faqs", func(r chi.Router) {
		r.Use(AuthMiddleware)
		r.Get("/", faqcontroller.GetFaq)
		r.Post("/", faqcontroller.CreateFaq)

		r.Route("/{faqId}", func(r chi.Router) {
			r.Use(FaqContext)
			r.Get("/", faqcontroller.GetFaqByID)
			r.Post("/", faqcontroller.UpdateFaq)
			r.Delete("/", faqcontroller.DeleteFaq)
		})
	})

	chiRoute.Route("/api/user/faqs/search", func(r chi.Router) {
		r.Get("/", faqcontroller.SearchFaq)
	})
	return chiRoute
}

// FaqContext is for processing faqId parameter in route
func FaqContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		faqID := chi.URLParam(r, "faqId")
		ctx := context.WithValue(r.Context(), "faqId", faqID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func allowOriginFunc(r *http.Request, origin string) bool {
	return true
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			http.Error(w, "Token not provided", 403)
			return
		}

		tokens := strings.Split(authHeader, " ")

		c := make(chan auth.AuthResult)
		go auth.Validate(tokens[1], c)
		result := <-c
		if result.Error != nil {
			http.Error(w, result.Error.Error(), 403)
			return
		}
		if result.Role != "Customer Service" {
			http.Error(w, "you don't have enough privileges", 403)
			return
		}

		next.ServeHTTP(w, r)
	})
}
