package app

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	_ "go-faq/internal/app/master/migrations" // importing migration package
	"go-faq/internal/app/master/providers/search"
	repository "go-faq/internal/app/master/repositories"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/go-pg/migrations/v7"
	"github.com/go-pg/pg/v9"
	"github.com/joho/godotenv"
)

var db pg.DB

// Run is main function, which split commands
func Run() {
	err := godotenv.Load()
	if err != nil {
		exitf(err.Error())
	}

	arguments := os.Args[1]
	db = *pg.Connect(&pg.Options{
		Addr:     os.Getenv("DB_ADDR"),
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Database: os.Getenv("DB_NAME"),
	})

	switch arguments {
	case "run":
		RunServer()
		break
	case "migration:init":
		MigrationRun("init")
		break
	case "migration:up":
		MigrationRun("up")
		break
	case "migration:down":
		MigrationRun("down")
		break
	case "migration:reset":
		MigrationRun("reset")
		break
	case "migration:version":
		MigrationRun("version")
	default:
		exitf("command not found")
	}
}

// RunServer run the web server
func RunServer() {
	client, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: getElasticAddresses(),
	})
	if err != nil {
		exitf(err.Error())
	}

	repository.Initialize(&db)
	search.Initialize(client)
	log.Fatal(http.ListenAndServe(":1000", Route()))
}

// MigrationRun is to run any migration command
func MigrationRun(args string) {
	oldVersion, newVersion, err := migrations.Run(&db, args)
	if err != nil {
		exitf(err.Error())
	}
	if newVersion != oldVersion {
		fmt.Printf("migrated from version %d to %d\n", oldVersion, newVersion)
	} else {
		fmt.Printf("version is %d\n", oldVersion)
	}
}

func exitf(s string, args ...interface{}) {
	errorf(s, args...)
	os.Exit(1)
}

func errorf(s string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, s+"\n", args...)
}

func getElasticAddresses() []string {
	addressraw := os.Getenv("ES_HOST")
	address := strings.Split(addressraw, ",")
	return address
}
