package models

import (
	"time"
)

type Faq struct {
	Id        int       `json:"id"`
	Orders    int64     `pg:"orders" json:"order"`
	Question  string    `json:"question"`
	Answer    string    `json:"answer"`
	CreatedAt time.Time `pg:"default:now()" json:"createdAt"`
	UpdatedAt time.Time `pg:"default:now()" json:"updatedAt"`
}
