package search

import (
	"go-faq/internal/app/master/models"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIndex(t *testing.T) {
	InitializeMock(ESMockService{})
	faqsearch := SearchProviders["FaqSearch"].(BaseSearch)

	t.Run("Success", func(t *testing.T) {
		var faq *models.Faq = &models.Faq{
			Id:       1,
			Orders:   1,
			Question: "Is Test?",
			Answer:   "Yes",
		}
		_, err := faqsearch.Index(faq)
		assert.Equal(t, err, nil, "Error should be nil")
	})
}
