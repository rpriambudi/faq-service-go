package search

import (
	"bytes"
)

type ESMockService struct{}

func (ss ESMockService) Index(index string, documentID string, body []byte) error {
	return nil
}

func (ss ESMockService) Delete(index string, documentID string) error {
	return nil
}

func (ss ESMockService) Search(index string, query bytes.Buffer) (interface{}, error) {
	return nil, nil
}
