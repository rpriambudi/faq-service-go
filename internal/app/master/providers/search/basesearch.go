package search

import (
	esclient "go-faq/pkg/elastic"
	"os"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
)

// SearchProviders is the placeholder for search services
var SearchProviders map[string]BaseSearch = make(map[string]BaseSearch)

func Initialize(client *elasticsearch.Client) error {
	essearch := esclient.ESSearchService{
		Client: client,
	}

	faqsearch := FaqSearch{
		client: essearch,
	}
	SearchProviders["FaqSearch"] = faqsearch
	return nil
}

func InitializeMock(mock esclient.ESClientService) {
	faqsearch := FaqSearch{
		client: mock,
	}
	SearchProviders["FaqSearch"] = faqsearch
}

type BaseSearch interface {
	Index(i interface{}) (interface{}, error)
	Delete(id string) error
	Search(query string) (interface{}, error)
}

func getElasticAddresses() []string {
	addressraw := os.Getenv("ES_HOST")
	address := strings.Split(addressraw, ",")
	return address
}
