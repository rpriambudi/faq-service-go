package search

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"go-faq/internal/app/master/models"
	esclient "go-faq/pkg/elastic"
	"strconv"

	"bitbucket.org/tunaiku/elasticsearch-helper/search"

	esapihelper "bitbucket.org/tunaiku/elasticsearch-helper"
	"github.com/elastic/go-elasticsearch/v7/esapi"
)

type FaqSearch struct {
	client esclient.ESClientService
}

type FaqSearchResult struct {
	Order    float64 `json:"order"`
	Question string  `json:"question"`
	Answer   string  `json:"answer"`
}

func (fs FaqSearch) Index(i interface{}) (interface{}, error) {
	faq := i.(*models.Faq)
	jsonObj, err := json.Marshal(faq)
	if err != nil {
		fmt.Printf("Data: %v", faq)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	err = fs.client.Index("faqs", strconv.Itoa(faq.Id), jsonObj)
	if err != nil {
		fmt.Printf("Data: %v", faq)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	return faq, nil
}

func (fs FaqSearch) Delete(id string) error {
	err := fs.client.Delete("faqs", id)
	return err
}

func (fs FaqSearch) Search(query string) (interface{}, error) {
	var result []FaqSearchResult

	queryObj, err := buildQuery(query)
	if err != nil {
		return nil, err
	}

	res, err := fs.client.Search("faqs", queryObj)
	if err != nil {
		return nil, err
	}

	esresponse := esapihelper.WrapClientResponse(res.(*esapi.Response))
	if esresponse.IsError() {
		return nil, errors.New(esresponse.Status())
	}

	response, err := search.WrapSearchResponse(esresponse)
	if err != nil {
		return nil, err
	}

	for _, r := range response.Hits.Hits {
		var mapper *FaqSearchResult = &FaqSearchResult{}
		r.MapDocument(mapper)
		result = append(result, *mapper)
	}

	return result, nil
}

func buildQuery(query string) (bytes.Buffer, error) {
	queryObj := map[string]interface{}{
		"query": map[string]interface{}{
			"match_all": map[string]interface{}{},
		},
	}
	if query != "" {
		queryObj = map[string]interface{}{
			"query": map[string]interface{}{
				"multi_match": map[string]interface{}{
					"query":  query,
					"fields": []string{"question", "answer"},
				},
			},
		}
	}

	var queryBuffer bytes.Buffer
	err := json.NewEncoder(&queryBuffer).Encode(queryObj)

	return queryBuffer, err
}

func (fsr *FaqSearchResult) MapSearchResult(d search.Document) {
	fsr.Order = d.Source["order"].(float64)
	fsr.Question = d.Source["question"].(string)
	fsr.Answer = d.Source["answer"].(string)
}
